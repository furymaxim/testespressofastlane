package com.furymaxim.testespressofastlane;


import androidx.test.core.app.ActivityScenario;

import org.junit.Before;

import org.junit.Test;

import tools.fastlane.screengrab.Screengrab;
import tools.fastlane.screengrab.UiAutomatorScreenshotStrategy;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class ActivitySimpleTest{

    @Before
    public void setUp() {
        ActivityScenario.launch(MainActivity.class);
        Screengrab.setDefaultScreenshotStrategy(new UiAutomatorScreenshotStrategy());
    }

    @Test
    public void simpleTest(){
        onView(withId(R.id.editA)).perform(typeText("stringA"),closeSoftKeyboard());
        Screengrab.screenshot("editAisFilled");
        onView(withId(R.id.editB)).perform(typeText("stringB"),closeSoftKeyboard());
        Screengrab.screenshot("editBisFilled");
        onView(withId(R.id.editC)).perform(typeText("stringC"),closeSoftKeyboard());
        Screengrab.screenshot("editCisFilled");
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.smile)).check(matches(isDisplayed()));
        Screengrab.screenshot("smileIsDisplayed");
    }

   /* private ActivitySimpleTest(Context appContext) {
        super(appContext);
    }*/
/*
    @Override
    protected File getScreenshotFile(File screenshotDirectory, String screenshotName) {
        String screenshotFileName = screenshotName + ".png";
        return new File(screenshotDirectory, screenshotFileName);
    }*/
}
