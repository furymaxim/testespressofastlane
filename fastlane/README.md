fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## Android
### android capture_screen
```
fastlane android capture_screen
```
Capture Screen
### android sharelane
```
fastlane android sharelane
```

### android send_mail
```
fastlane android send_mail
```

### android upload_to_slack
```
fastlane android upload_to_slack
```
Upload the APK to Slack channel
### android upload_images_to_slack
```
fastlane android upload_images_to_slack
```
Upload images to Slack channel

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
